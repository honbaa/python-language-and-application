# (1) 一个小时有多少秒？这里，请把交互式解释器当作计算器使用，将每分钟的秒数（60）乘以每小时的分钟数（60）得到结果。
print(60 * 60)
# (2) 将上一个练习得到的结果（每小时的秒数）赋值给名为 seconds_per_hour 的变量。
seconds_per_hour = 60 * 60
# (3) 一天有多少秒？用你的 seconds_per_hour 变量进行计算。
print(seconds_per_hour * 24)
# (4) 再次计算每天的秒数，但这一次将结果存储在名为 seconds_per_day 的变量中。
seconds_per_day = seconds_per_hour * 24
# (5) 用 seconds_per_day 除以 seconds_per_hour，使用浮点除法（/）。
print(seconds_per_day / seconds_per_hour)
# (6) 用 seconds_per_day 除以 seconds_per_hour，使用整数除法（//）。除了末尾的 .0，本练习所得结果是否与前一个练习用浮点数除法得到的结果一致？
print(seconds_per_day // seconds_per_hour)